<?php
define('DB_DRIVER','mysql');
define('DB_HOST','localhost');
define('DB_NAME','anmakarov');
define('DB_USER','anmakarov');
define('DB_PASS','neto1704');
    // Функция для проверки данных, полученных от пользователя
    $pattern = "/^[0-9a-z_]+$/i";
    $patternType = "/^[0-9a-z()]+$/i";
    function check_data($pattern, $value_user){
      if (!preg_match($pattern, $value_user)) {
        die("Допустимо использовать только латинские буквы, цифры и символ нижнего подчеркивания!");
      }
    }

    try {

      $connect_str = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
      $db = new PDO($connect_str, DB_USER, DB_PASS);
      $post = $_POST;

      if (isset($post['table'])) {
        $thisTable = $post['table'];
        if (isset($_POST['new_name']) && isset($_POST['field']) && isset($post['type'])) {
          check_data($pattern, $post['new_name']);
          echo $sql = "ALTER TABLE $thisTable CHANGE $post[field] $post[new_name] $post[type]";
          $sql_rename = $db->prepare("ALTER TABLE $thisTable CHANGE $post[field] $post[new_name] $post[type]")->execute();

        }

        if (isset($_POST['new_type']) && isset($_POST['field'])){
          check_data($patternType, $post['new_type']);
          echo $sql = "ALTER TABLE $thisTable MODIFY  $post[field] $post[new_type]";
          $sql_rename = $db->prepare("ALTER TABLE $thisTable MODIFY  $post[field] $post[new_type]")->execute();

        }

        if (isset($post['delete'])) {
          $sql_delete = $db->prepare("ALTER TABLE $thisTable DROP COLUMN $post[delete]")->execute();
        }

        $sqlDescribe = $db->prepare("DESCRIBE {$thisTable}");
        $sqlDescribe->execute();
        $tableData = $sqlDescribe->fetchAll(PDO :: FETCH_ASSOC);
      } else {
        $tableName = "test_table";
        $sqlCreate = "
                DROP TABLE IF EXISTS `{$tableName}`;
            
                CREATE TABLE `{$tableName}` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `name` varchar(255) NOT NULL,
                    `price` int(11) NOT NULL,
                    `category` varchar(255) NOT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ";
        $db->prepare($sqlCreate)->execute(); // ? $message = '- таблица test_table создана' : $message = '- ошибка при создании таблицы';

        $arrayKey = "Tables_in_" . DB_NAME;

        if ($result = $db->query("SHOW TABLES")) {
          foreach ($result as $row) {
            $tablesData[] = $row[$arrayKey];
          }
        }
      }

    } catch (Exception $e) {
      die('Error: ' . $e->getMessage() . '<br/>');
    }

    require_once 'base.php';


<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="block">
  <?php if (isset($tableData)) { ?>
  <h3>Данные о полях таблицы <?= $thisTable ?></h3>
  <table>
    <tr>
      <?php
        foreach($tableData as $title) {
          foreach ($title as $key => $value) { ?>
      <th><?= $key ?></th>
      <?php }break;} ?>
      <th>Изменить/Удалить</th>
    </tr>

    <?php
      foreach ($tableData as $val) { ?>
    <tr>
      <?php foreach ($val as $key => $data) {
         $tableResultData[$key] = $data; ?>
      <td>
        <?= $data ?>
      </td>
      <?php } ?>

      <td>
        <form action="index.php" method="POST">
          <input type="text" name="new_name" placeholder="Новое название поля">
          <input type="hidden" name="table" value="<?= $thisTable ?>">
          <input type="hidden" name="field" value="<?= $tableResultData['Field'] ?>">
          <input type="hidden" name="type" value="<?= $tableResultData['Type'] ?>">
          <input type="submit"  value="Изменить имя поля">
        </form>
        <form action="index.php" method="POST">
          <input type="text" name="new_type" placeholder="Новый тип">
          <input type="hidden" name="table" value="<?= $thisTable ?>">
          <input type="hidden" name="field" value="<?= $tableResultData['Field'] ?>">
          <input type="submit"  value="Изменить тип">
        </form>
        <form action="index.php" method="POST">
          <input type="hidden" name="table" value="<?= $thisTable ?>">
          <input type="hidden" name="delete" value="<?= $tableResultData['Field'] ?>">
          <input type="submit" style="background-color: lightcoral"  value="Удалить поле">
        </form>
      </td>
    </tr>
    <?php } ?>
  </table>
  <div class="mar">
    <span><a href="index.php">Вернуться к списку таблиц</a></span>
  </div>
</div>
<?php } else { ?>
<div class="block">
  <h2>DATABASE <?= DB_NAME ?></h2>
  <span style="text-align: center">Убедительная просьба, дабы не сломать предыдущую домашнюю работу, тестировать на таблице <b><?= $tableName ?></b></span>
  <table>
    <tr>
      <th>Название таблицы</th>
      <th>Перейти к просмотру таблицы</th>
    </tr>
    <?php
      foreach ($tablesData as $tables) {
        if ($tables == $tableName){
    ?>
    <tr style="background-color: greenyellow ">
      <?php }else{ ?>
    <tr style="background-color: lightcoral ">
      <?php } ?>
      <td>
        <?= $tables ?>
      </td>
      <td>
        <form action="index.php" method="POST">
          <input type="hidden" name="table" value="<?= $tables ?>">
          <input type="submit" value="Перейти к <?= $tables ?>">
        </form>
      </td>
    </tr>
    <?php }} ?>
  </table>


</div>
</body>
</html>
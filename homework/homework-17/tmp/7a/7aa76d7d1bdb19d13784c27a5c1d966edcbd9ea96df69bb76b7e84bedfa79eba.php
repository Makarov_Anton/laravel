<?php

/* task.twig */
class __TwigTemplate_e579ac24eadb2cec90498842460eee9ddd4f097864078988b62c24ec46f08cd8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!doctype html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\">
    <title></title>

    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>

</head>
<body>

<h1>Добро пожаловать, <?= \$_SESSION['login'] ?>. Твой список дел на сегодня.</h1>
<div style=\"float: left\">
    <form method=\"POST\">

        <input type=\"text\" name=\"description\" placeholder=\"Описание задачи\" value=\"<?= \$value ?>\">
        <input type=\"submit\" name=\"save\" value=\"Добавить\">

    </form>
</div>
<div style=\"float: left; margin-left: 20px;\">
    <form method=\"POST\">
        <label for=\"sort\">Сортировать по:</label>
        <select name=\"sort_by\">
            <option value=\"date_created\">Дате добавления</option>
            <option value=\"is_done\">Статусу</option>
            <option value=\"description\">Описанию</option>
        </select>
        <input type=\"submit\" name=\"sort\" value=\"Отсортировать\"/>
    </form>
</div>
<div style=\"clear: both\"></div>

<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>
    <tr>
        <?php
      if (empty(\$data)){
        echo \"Дел пока нет, кайфуй.\";
      }else{
      foreach (\$data as \$date){

      \$status = (\$date['is_done'] == 0) ? \"<span style='color: orange;'>В процессе</span>\" : \"<span style='color: green;'>Выполнено</span>\";
        ?>
        <?php if (isset(\$date['description']) && strlen(\$date['description']) > 3){ ?>
    <tr>
        <td><?= \$date['description'] ?></td>
        <td><?= \$date['date_added'] ?></td>
        <td><?= \$status ?></td>
        <td>
            <a href='?id=<?= \$date['id'] ?>&action=edit'>Изменить</a>
            <a href='?id=<?= \$date['id'] ?>&action=done'>Выполнить</a>
            <a href='?id=<?= \$date['id'] ?>&action=delete'>Удалить</a>
        </td>
        <td><?= \$date['assigned'] ?></td>
        <td><?= \$date['author'] ?></td>
        <td>
            <form method=\"POST\">
                <select name=\"assigned_user_id\">
                    <?php foreach (\$users as \$user) { ?>
                    <option value=\"<?= \"{\$date['id']};{\$user}\" ?>\"><?= \$user ?></option>
                    <?php } ?>
                </select>
                <input type=\"submit\" name=\"assign\" value=\"Переложить ответственность\">
            </form>
        </td>
    </tr>
    </tr>
    <?php }}} ?>

</table>
<div>
    <h3>Также, посмотрите, что от Вас требуют другие люди:</h3>
</div>
<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
    </tr>
    <tr>
        <?php
    if (empty(\$datas)){
      echo \"Дел пока нет, кайфуй.\";
    }else{
    foreach (\$datas as \$dates){
      \$status = (\$dates['is_done'] == 0) ? \"<span style='color: orange;'>В процессе</span>\" : \"<span style='color: green;'>Выполнено</span>\";
        ?>
        <?php if (isset(\$dates['description']) && strlen(\$dates['description']) > 3){ ?>
    <tr>
        <td><?= \$dates['description'] ?></td>
        <td><?= \$dates['date_added'] ?></td>
        <td><?= \$status ?></td>
        <td>
            <a href='?id=<?= \$dates['id'] ?>&action=edit'>Изменить</a>
            <a href='?id=<?= \$dates['id'] ?>&action=done'>Выполнить</a>
            <a href='?id=<?= \$dates['id'] ?>&action=delete'>Удалить</a>
        </td>
        <td><?= \$_SESSION['login'] ?></td>
        <td><?= \$dates['author'] ?></td>
    </tr>
    </tr>
    <?php }}} ?>

</table>

<div style=\"font-weight: bold; font-size: 14px; color: #1e7e34; margin: 50px 0;\">
    <a href=\"../controller/controller_admin.php?logout=1\">Выйти</a>
</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "task.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "task.twig", "D:\\netology\\homework\\homework-17\\templates\\task.twig");
    }
}

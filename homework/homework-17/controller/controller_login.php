<?php
require_once "../model/function.php";
$twig = twig("../");
try
{
  $params = array();
  $db = dbConnect();
  $post = addslash($_POST);
  if (isset($post['login'])) {
    login($post, $db);
  }
  if (isset($post['register'])) {
    register($post, $db);
  }

  echo $twig->render('login.twig', $params);
}

catch(PDOException $e)
{
  die("Error: ".$e->getMessage());
}


<?php

function edit($db, $get)
{
  $sqlEdit = ("select description from task WHERE id = {$get['id']}");

  if ($res = $db->query($sqlEdit)) {
    foreach ($res as $row) {
      $value = $row['description'];
    }
  }
  return $value;
}

function done($db, $get)
{
  $data['is_done'] = 1;
  $sqlDone = ("update task SET is_done = 1 WHERE id = {$get['id']}");
  $db->exec($sqlDone);
  return $data['is_done'];
}

function delete($db, $get)
{
  $sqlDelete = ("DELETE FROM task WHERE id = {$get['id']}");
  $db->exec($sqlDelete);
}

function update ($db, $get, $post)
{
  $sql = ("update task SET description = '{$post['description']}' where description = '{$value}' AND id = {$get['id']}");
  $db->exec($sql);
}

function insert($db, $user, $post)
{
  $sqlInsert = ("INSERT INTO task (description, date_added, user_id, assigned_user_id) VALUE ('{$post['description']}', NOW(),'{$user['id']}', {$user['id']})");
  $db->exec($sqlInsert);
}

function update_assigned_user($db, $assigned_user, $task_id){
  $result = $db->query("SELECT id FROM user WHERE login = '{$assigned_user}'");
  foreach ($result as $row) {
    $db->query("UPDATE task SET assigned_user_id = '{$row['id']}' where id = '{$task_id}'");
  }
}

function sql_author()
{
  return "SELECT u2.id AS user_id, task.id, description, is_done, date_added, u1.login AS author, u2.login AS assigned FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id where u1.login = '{$_SESSION['login']}'";
}

function sql_assigned()
{
  return  "SELECT u2.id AS user_id, task.id, description, is_done, date_added, u1.login AS author, u2.login AS assigned FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id where u2.login = '{$_SESSION['login']}' and u1.login != '{$_SESSION['login']}';";
}

function users($db)
{
  $sql = "select login from user";
  if ($result = $db->query($sql)) {
    foreach ($result as $row) {
      $users[] = $row['login'];
    }
  }
  return $users;
}


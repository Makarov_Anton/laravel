<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
ini_set('display_errors', 1);
session_start();

function twig($path){
  require_once $path . 'vendor/autoload.php';
  $templates = $path . "templates";
  $tmp = $path . "tmp";
  $loader = new Twig_Loader_Filesystem($templates);
  $twig = new Twig_Environment($loader, array(
    'cache' => $tmp,
    'auto_reload' => true,
  ));
  return $twig;
}

function login($post, $db){
  if (strlen($post['login']) < 4){
    echo "Количество симвлов должно быть больше 3!";
  }else {
    $_SESSION['login'] = $post['login'];
    $post['password'] = md5($post['password']);
    if (isset($post['sign_in'])) {
      if ($sql = ("select password from user WHERE login = '{$post['login']}'")) {
        if ($result = $db->query($sql)) {
          foreach ($result as $row) {
            $pass = $row['password'];
          }
        }

        if (isset($post['password'])) {
          if ($pass == $post['password']) {
            redirect('controller_task');
            exit;
          } else {
            echo "Пользователя не существует или введён неверный пароль!";
          }
        }
      }
    }
  }
}

function register($post, $db){
  $reg = 1;
  $result = $db->query("SELECT login, password  FROM `user`");
  foreach ($result as $row) {
    if ($row['login'] == $post['login']){
      echo "Такой логин уже существует!";
      $reg = 0;
      break;
    }
  }

  if (isset($post['password'])) {
    if (strlen($post['password']) < 4){
      echo "Количество симвлов должно быть больше 3!";
    }else {
      if ($reg == 1) {
        $sqlInsert = ("INSERT INTO user (login, password) VALUE ('{$post['login']}', '{$post['password']}')");
        echo "Регистрация прошла успешно";
        $db->exec($sqlInsert);
      }
    }
  }
}




function logout(){
  session_destroy();
  redirect("controller_login");
}

function dbConnect()
{
//  define('DB_DRIVER','mysql');
//  define('DB_HOST','localhost');
//  define('DB_NAME','anmakarov');
//  define('DB_USER','anmakarov');
//  define('DB_PASS','neto1704');

  define('DB_DRIVER', 'mysql');
  define('DB_HOST', '127.0.0.1');
  define('DB_NAME', 'books');
  define('DB_USER', 'mysql');
  define('DB_PASS', 'mysql');
  $connect_str = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
  $db = new PDO($connect_str, DB_USER, DB_PASS);
  return $db;
}


function addslash($array)
{
  $arr = array();
  foreach ($array as $key => $value) {
    $arr[$key] = addslashes($value);
  }
  return $arr;
}

function sort_print_task($post, $temp_sql)
{

    switch ($post['sort_by']) {
      case "date_created":
        $sql = $temp_sql . " ORDER BY date_added";
        break;
      case "is_done":
        $sql = $temp_sql . " ORDER BY is_done";
        break;
      case "description":
        $sql = $temp_sql . " ORDER BY description";
        break;
    }
    return $sql;

}

function print_task($sql, $db)
{
  $data = array();
  if ($result = $db->query($sql)) {
    foreach ($result as $row) {
      $data[] = array(
        "id" => $row['id'],
        "user_id" => $row['user_id'],
        "date_added" => $row['date_added'],
        "description" => $row['description'],
        "is_done" => $row['is_done'],
        "author" => $row['author'],
        "assigned" => $row['assigned'],
      );
    }

    return $data;
  }
}

function redirect($page)
{
  header("Location: $page.php");
  die;
}
<?php
/**
 * Created by PhpStorm.
 * User: Sec
 * Date: 02.03.2018
 * Time: 12:18
 */

namespace classes;


class bag
{
  public $items=[];

  public function add($bagItem)
  {
    if(isset($this->items[$bagItem->name])) {
      $bagItem->quantity += $this->items[$bagItem->name]->quantity;
    }
    $this->edit($bagItem);
  }

  public function edit($bagItem)
  {
    $bagItem->subtotal();
    $this->items[$bagItem->name] = $bagItem;
  }


  public function delete($name)
  {
    unset($this->items[$name]);
  }

}
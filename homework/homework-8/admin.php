<?php
session_start();

if (!isset($_SESSION['user'])){
  header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true);
  die;
}
$quest = ["name", "question", "answer1", "answer2", "answer3", "answer4", "answer5", "true"];
if (isset($_FILES) && !empty($_FILES)) {
  $array = reArrayFiles($_FILES['userfile']);

  $path = __DIR__ . "/test/";
  foreach ($array as $file) {
    $uploadfile = basename($file['name']);
    $file_extension = pathinfo($file['name'], PATHINFO_EXTENSION);
    if ($file['error'] == UPLOAD_ERR_OK && $file_extension == "json") {
      $content = file_get_contents($file['tmp_name']);
      $tmp = json_decode($content, true);
      check($quest, $tmp);
      $way = $path . $uploadfile;
      move_uploaded_file($file['tmp_name'], $way);

      if (file_exists($way )){
        header('Location:list.php');
        exit;
      }


    }else {
      echo "<p style='color:red'> ОШИБКА - {$file['name']} НЕ ЗАГРУЖЕН </p>";
    }
  }
}

function check($quest, $tmp){
  foreach ($tmp as $value){
    $counts = count($value);
    if ($counts !== 8) {
      echo "Проверьте количесво полей. Пример : <br>";
      help($quest);
      exit;
    }
    foreach ($value as $key => $values){
      if (!in_array($key,$quest)) {
        echo "Проверьте структуру файла! Ключ<b> [" . $key . "] </b>не верен!" . "<br> Ознакаомьтес с разрешённомы ключами и их последовательность:<br>";
        help($quest);
        exit;
      }
    }
  }
}

function help($quest){
  foreach ($quest as $k => $v){
    echo "[" . $k . "] => " . $v . "<br>";
  }
}

function reArrayFiles($files) {
  $array = array();
  $count = count($files['name']);
  $keys = array_keys($files);

  for($i = 0; $i < $count; $i++) {
    foreach($keys as $key) {
      $array[$i][$key] = $files[$key][$i];
    }
  }

  return $array;
}


?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>download form</title>
</head>
<body>
<div style="font-weight: bold; font-size: 18px; color: #1e7e34; margin: 20px;">
  <a href="list.php">К загруженным тестам!</a>
</div>
<form action=" " method="post" enctype="multipart/form-data">
  <input type="file" name="userfile[]"><br>
  <input type="submit" value="Загрузить"><br>
</form>
</body>
</html>
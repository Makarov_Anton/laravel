<?php
//session_start();
require_once 'functions.php';
$path = __DIR__ . "/test/";
$list = array_diff( scandir( $path), array('..', '.'));
$delete = $_POST;
if (empty($list)){
  echo "<p style='color: red;font-size: 20px;margin-left: 20px;'><b>Папка с тестами пуста.</b></p>";
}
if (!empty($delete)) {

  foreach ($delete as $del) {
    unlink($path . $del);
  }
  redirect('list');
}


?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>list</title>
</head>
<body>
<?php if (isset($_SESSION['user'])){ ?>
<div style="font-weight: bold; font-size: 18px; color: #1e7e34; margin: 20px;">
  <a href="admin.php">Загрузить новые тесты</a>
</div>
<?php } ?>
  <span style="margin-left: 20px"><b>Выберите тест:</b></span>
<?php if (isset($_SESSION['user'])){ ?>
  <span style="margin-left: 20px">Удалить тест:</span>
<?php } ?>
<form method="post">
  <ol>
    <?php foreach ($list as $value) { ?>
    <li>
      <a href="test.php?name=<?= $value ?> "> <?= $value ?></a>
      <?php if (isset($_SESSION['user'])){ ?>
          <input type="checkbox" name="<?= $value ?>" value="<?= $value ?>" style="margin-left: 80px">
      <?php } ?>
    </li>
    <?php } ?>
  </ol>

  <?php if (isset($_SESSION['user'])){ ?>
  <input type="submit" value="Submit" />
  <?php } ?>

</form>
<div style="font-weight: bold; font-size: 14px; color: #1e7e34; margin: 50px;">
  <a href="logout.php">Выйти</a>
</div>

</body>
</html>

